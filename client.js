/* global TrelloPowerUp */

var Promise = TrelloPowerUp.Promise;

var WHITE_ICON = 'https://cdn.glitch.com/62b96c4e-5d6a-4c94-b24d-0d286981af16%2FLogoWhite.svg?1528878129874';
var BLACK_ICON = 'https://cdn.glitch.com/62b96c4e-5d6a-4c94-b24d-0d286981af16%2FLogoBlack.svg?1528878131172';

TrelloPowerUp.initialize({
  // Start adding handlers for your capabilities here!
   'board-buttons': function(t, options) {
   	return [{
   		icon: {
        dark: WHITE_ICON,
        light: BLACK_ICON
      },
   		text: 'Number Stats',
      callback: function(t) {
        
        var fields = { };
        
        t.board('customFields').then(function (board) {
          //console.log(JSON.stringify(board, null, 2));
          board.customFields.forEach(function(field) {
            if(field.type == "number") {
              fields[field.id] = { "name": field.name, "sum": 0, "count": 0, "avg": 0 };
            }
          });
        });
        
        //console.log(fields);
        
        return t.cards('all')
        .then(function (cards) {
          cards.forEach(function(card) {
            //console.log(card);
            card.customFieldItems.forEach(function(customField) {
              //console.log(customField);
              if(fields[customField.idCustomField] != undefined) {
                fields[customField.idCustomField].count++;
                fields[customField.idCustomField].sum += parseFloat(customField.value.number);
              }
            });
          });
          
          for (var key in fields) {
            if(!fields.hasOwnProperty(key)) continue;
            if(fields[key].count > 0) {
              fields[key].avg = (fields[key].sum / fields[key].count).toFixed(2);
            }
          };
          
          //console.log(fields);
          
          var height = 190;
          
          if(Object.keys(fields).length > 1) {
            height = 360;
          }
          
          t.popup({
            title: "Number Stats",
            url: 'popup.html',
            args: { myArgs: 'You can access these with t.arg()', fields: fields },
            height: height
          });
        });
      }
   	}];
   },
});
