var express = require('express');
var cors = require('cors');
var rp = require('request-promise');

var app = express();

// your manifest must have appropriate CORS headers, you could also use '*'
app.use(cors({ origin: 'https://trello.com' }));

// http://expressjs.com/en/starter/static-files.html
app.use(express.static('public'));

// Keep Glitch from sleeping by periodically sending ourselves a http request
setInterval(function() {
  console.log('❤️ Keep Alive Heartbeat');
  rp('https://glitch.com/#!/project/trellocardsnooze')
  .then(() => {
    console.log('💗 Successfully sent http request to Glitch to stay awake.');
  })
  .catch((err) => {
    console.error(`💔 Error sending http request to Glitch to stay awake: ${err.message}`);
  });
}, 150000); // every 2.5 minutes

app.get("*", function (request, response) {
  response.sendFile(__dirname + '/views/index.html');
});

// listen for requests :)
var listener = app.listen(process.env.PORT, function () {
  console.log('Your app is listening on port ' + listener.address().port);
});
