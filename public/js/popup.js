/* global TrelloPowerUp */

var t = TrelloPowerUp.iframe();

var f = t.arg("fields", "test");

      /*<fieldset>
        <legend>Field 1:</legend>
        <div class="card-detail-item">
          <h3 class="card-detail-item-header">
          Count:
          </h3>
          <input class="card-detail-badge is-clickable js-custom-field-detail-badge" size="7" title="1" type="text" value="1" readonly>
        </div>
        <div class="card-detail-item">
          <h3 class="card-detail-item-header">
          Count:
          </h3>
          <input class="card-detail-badge is-clickable js-custom-field-detail-badge" size="7" title="1" type="text" value="1" readonly>
        </div>
        <div class="card-detail-item">
          <h3 class="card-detail-item-header">
          Count:
          </h3>
          <input class="card-detail-badge is-clickable js-custom-field-detail-badge" size="7" title="1" type="text" value="1" readonly>
        </div>
     </fieldset>*/
//console.log(f);


for (var key in f) {
  if(!f.hasOwnProperty(key)) continue;
  //console.log("Creating field: " + f[key].name + ".");
  
  var fs = document.createElement("fieldset");
  var legend = document.createElement("legend");
  legend.innerHTML = f[key].name;
  fs.appendChild(legend);
  var div1 = document.createElement("div");
  var div2 = document.createElement("div");
  var div3 = document.createElement("div");
  
  div1.classList.add("card-detail-item-header");
  div2.classList.add("card-detail-item-header");
  div3.classList.add("card-detail-item-header");

  var header1 = document.createElement("h3");
  var header2 = document.createElement("h3");
  var header3 = document.createElement("h3");
  
  header1.classList.add("card-detail-item-header");
  header2.classList.add("card-detail-item-header");
  header3.classList.add("card-detail-item-header");
  
  header1.innerHTML = "Count:";
  header2.innerHTML = "Sum:";
  header3.innerHTML = "Average:";
  
  div1.appendChild(header1);
  div2.appendChild(header2);
  div3.appendChild(header3);
  
  var input1 = document.createElement("input");
  var input2 = document.createElement("input");
  var input3 = document.createElement("input");
  
  input1.classList.add("card-detail-badge", "is-clickable", "js-custom-field-detail-badge");
  input2.classList.add("card-detail-badge", "is-clickable", "js-custom-field-detail-badge");
  input3.classList.add("card-detail-badge", "is-clickable", "js-custom-field-detail-badge");
  
  input1.setAttribute("size", "7");
  input2.setAttribute("size", "7");
  input3.setAttribute("size", "7");
  
  input1.setAttribute("title", f[key].count);
  input2.setAttribute("title", +f[key].sum.toFixed(2));
  input3.setAttribute("title", f[key].avg);
  
  input1.setAttribute("type", "text");
  input2.setAttribute("type", "text");
  input3.setAttribute("type", "text");
  
  input1.setAttribute("value", f[key].count);
  input2.setAttribute("value", +f[key].sum.toFixed(2));
  input3.setAttribute("value", f[key].avg);
  
  input1.setAttribute("disabled", "");
  input2.setAttribute("disabled", "");
  input3.setAttribute("disabled", "");
  
  div1.appendChild(input1);
  div2.appendChild(input2);
  div3.appendChild(input3);
  
  fs.appendChild(div1);
  fs.appendChild(div2);
  fs.appendChild(div3);

  window.fields.appendChild(fs);
}